Création comptes utilisateurs avec authentification par clés SSH 
==========

Rôle permettant de créer des comptes utilisateurs, déployer leurs clés publiques et configurer un accès sudo sans mot de passe. 

Testé sur Debian 11.5 et Rocky Linux 9.1

Liste des actions effectuées par le rôle user_accounts : 

  * Test de connexion.
  * Création du groupe technicien.
  * Création des utilisateurs.
  * Ajout des utilisateurs aux groupes sudo et technicien.
  * Copie des clés publiques.
  * Activation de sudo sans mot de passe pour les utilisateurs du groupe technicien.
  * Modification fichier configuration SSH : 
     * Désactivation des connexions SSH pour le compte root.
     * Désactivation de l'authentification par mot de passe.
     * Activation de l'authentification par clé publique.
  * Rechargement configuration SSH.


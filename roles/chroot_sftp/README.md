Configuration serveur SFTP chrooté avec authentification par clés SSH
==========

Rôle permettant de créer un serveur sftp chrooté avec authentification par clés SSH. 

Testé sur Debian 11.5 et Rocky Linux 9.1

Liste des actions effectuées par le rôle chroot_sftp : 

  * Test de connexion.
  * Création des répertoires personnels.
  * Création du groupe sftp.
  * Création des utilisateurs.
  * Modifications des permissions sur les répertoires personnels.
  * Copie des clés publiques.
  * Modification fichier configuration SSH : 
     * Désactivation des connexions SSH pour le compte root.
     * Désactivation de l'authentification par mot de passe.
     * Activation de l'authentification par clé publique.
     * Configuration du serveur SFTP interne.
  * Rechargement configuration SSH.



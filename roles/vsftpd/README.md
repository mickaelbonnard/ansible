Configuration serveur FTP avec vsFTPd
==========

Rôle permettant de configurer un serveur FTP avec VsFTPd.

Les mots de passe des utilisateurs sont stockés dans un vault.   

Testé sur Debian 11.5 et Rocky Linux 9.1

Liste des actions effectuées par le rôle vsftpd sur Debian : 

  * Test de connexion.
  * Mise à jour des dépôts.
  * Mise à jour des paquets.
  * Installation de vsftpd et openssl.
  * Création du répertoire openssl.
  * Création du certificat openssl.
  * Ouverture des ports dans ufw.
  * Création des utilisateurs.
  * Modification fichier configuration vsftpd.
  * Configuration liste utilisateurs chrootés.
  * Modification fichier shells.
  * Rechargement configuration vsftp.

Liste des actions effectuées par le rôle vsftpd sur Rockylinux : 

  * Test de connexion.
  * Mise à jour des dépôts.
  * Mise à jour des paquets.
  * Installation de vsftpd et openssl.
  * Création du répertoire openssl.
  * Création du certificat openssl.
  * Ouverture des ports dans firewalld.
  * Création des utilisateurs.
  * Modification fichier configuration vsftpd.
  * Configuration liste utilisateurs chrootés.
  * Modification fichier shells.
  * Rechargement configuration vsftp.




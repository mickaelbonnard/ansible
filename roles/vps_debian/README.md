Déploiement serveur Debian 
==========

Rôle permettant de configurer une Debian sur un VPS.

Testé sur Debian 11.5

Liste des actions effectuées par le rôle vps_debian : 

  * Test de connexion.
  * Mise à jour des dépôts.
  * Mise à jour des paquets.
  * Installation de fail2ban, ufw, postfix et s-nail.
  * Création de l'utilisateur.
  * Modification fichier configuration sudoers.
    * Activation de sudo sans mot de passe pour l’utilisateur.
  * Copie de la clé publique.
  * Autoriser les connexions ssh dans ufw.
  * Activation de ufw.
  * Modification fichier configuration ssh.
    * Désactivation des connexions ssh pour le compte root.
    * Désactivation de l'authentification par mot de passe.
    * Activation de l'authentification par clé publique.
  * Modification fichier de configuration fail2ban.
  * Modification fichier de configuration postfix.
  * Copie des fichiers .sasl_passwd.db et smtp_header_checks pour postfix.
  * Modification du fichier hosts.
  * Modification du hostname.
  * Rechargement configuration ssh, postfix et fail2ban.
  * Désactivation utilisateur ansible.


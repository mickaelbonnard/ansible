Déploiement serveur Apache avec certificats auto-signés 
==========

Rôle permettant d'installer un serveur Apache sur Debian et Rocky Linux et des virtualhost en HTTPS avec des certificats auto-signés.

Testé sur Debian 11.5 et Rocky Linux 9.1

Liste des actions effectuées par le rôle apache_openssl sur Debian :

  * Test de connexion.
  * Mise à jour des dépôts.
  * Mise à jour des paquets.
  * Installation de Apache et OpenSSL.
  * Activation modules ssl et rewrite.
  * Création du répertoire openssl.
  * Création du certificat openssl.
  * Création du dossier racine et définition des autorisations
  * Modification du fichier index.html.
  * Ouverture des ports dans ufw.
  * Désactivation du virtualhost 000-​default.conf.
  * Création du virtualhost srvdebian.conf.
  * Activation du virtualhost.
  * Test de la configuration
  * Rechargement configuration apache.

Liste des actions effectuées par le rôle apache_openssl sur Rocky Linux :

  * Test de connexion.
  * Mise à jour des dépôts.
  * Mise à jour des paquets.
  * Installation de Apache et mod_ssl.
  * Lancement du service et activation du service au démarrage.
  * Ouverture des ports dans firewalld.
  * Création du répertoire openssl.
  * Création du certificat openssl.
  * Création du virtualhost srvrockylinux.conf.
  * Création du dossier racine et définition des autorisations.
  * Modification du fichier index.html.
  * Test de la configuration.
  * Rechargement configuration apache.


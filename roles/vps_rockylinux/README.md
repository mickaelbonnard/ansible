Déploiement serveur Rocky Linux
==========

Rôle permettant de configurer une Rocky Linux sur un VPS.

Testé sur Rocky Linux 8.5 et 9.1 

Liste des actions effectuées par le rôle vps_rockylinux : 

  * Test de connexion.
  * Mise à jour des dépôts.
  * Mise à jour des paquets.
  * Installation du dépôt epel-release.
  * Installation de fail2ban, firewalld, postfix, cyrus-sasl-plain, mutt.
  * Création de l'utilisateur.
  * Copie de la clé publique.
  * Modification fichier configuration sudoers.
    * Activation de sudo sans mot de passe pour l’utilisateur. 
  * Modification fichier configuration ssh.
    * Désactivation des connexions ssh pour le compte root.
    * Désactivation de l'authentification par mot de passe.
    * Activation de l'authentification par clé publique.
  * Modification fichier de configuration fail2ban.
  * Modification fichier de configuration postfix.
  * Copie des fichiers .sasl_passwd.db et smtp_header_checks pour postfix.
  * Démarrage des services et activation des services au démarrage.
  * Suppression des services cockpit et dhcpv6-client dans firewalld.
  * Modification du fichier hosts.
  * Modification du hostname.
  * Rechargement configuration ssh.
  * Désactivation utilisateur ansible.
